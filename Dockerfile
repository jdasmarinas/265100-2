FROM python:3.9-slim-buster as base

## Invalidate old cache
RUN echo "this is a test message" > /tmp/test3


## Just try to install some packages
RUN apt-get update && apt-get install -y --no-install-recommends \
    locales \
    && rm -rf /var/lib/apt/lists/* \
    && locale-gen "en_US.UTF-8" \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

ENV LC_ALL="en_US.UTF-8" LANG="en_US.utf8"

RUN apt-get update && apt-get install -y --no-install-recommends \
    apt-utils \
    ca-certificates \
    curl \
    g++ \
    gnupg2 \
    parallel \
    git-core \
    wget

RUN echo "this is a test message" > /tmp/test

FROM base as ops

## Just try to install some packages
RUN curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-371.0.0-linux-x86_64.tar.gz && \
    tar -xzf google-cloud-sdk-371.0.0-linux-x86_64.tar.gz && \
    bash google-cloud-sdk/install.sh

FROM ops

RUN echo "changed" > /tmp/test2
